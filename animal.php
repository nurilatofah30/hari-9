<?php
class Animal {
    public $name,
           $legs,
           $cold_blooded;
  
    public function __construct($name, $legs, $cold_blooded) {
      $this->name = $name; 
      $this->legs = $legs; 
      $this->cold_blooded = $cold_blooded; 
    }
  }

             $sheep = new Animal("shaun", "4" , "no");
              echo "Name : $sheep->name"; // "shaun" 
              echo "<br>";
              echo "legs : $sheep->legs"; // 4
              echo "<br>";
              echo "cold blooded : $sheep->cold_blooded"; // "no"
              echo "<br>";
              echo "<br>";
               
?>